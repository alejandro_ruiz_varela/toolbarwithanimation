﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using com.refractored.fab;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;

namespace ExpandibleHeader
{
	[Activity (Label = "ExpandibleHeader", MainLauncher = true, Theme = "@style/MyTheme", Icon = "@drawable/icon")]
	public class MainActivity : ActionBarActivity, AbsListView.IOnScrollListener
	{
		#region AbsListView.IOnScrollListener Implementation
		public void OnScroll (AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
		{
			int scrollY = getScrollY(view);

			// This will collapse the header when scrolling, until its height reaches
			// the toolbar height
			HeaderView.TranslationY = Math.Max(0, scrollY + MinHeaderTranslation);

			// Scroll ratio (0 <= ratio <= 1). 
			// The ratio value is 0 when the header is completely expanded, 
			// 1 when it is completely collapsed
			float offset = 1 - Math.Max(
				(float) (-MinHeaderTranslation - scrollY) / -MinHeaderTranslation, 0f);

			// Update items alpha from offset
			UpdateItemsAlpha(offset);
		}

		public void OnScrollStateChanged (AbsListView view, ScrollState scrollState)
		{
			//throw new NotImplementedException ();
		}

		void UpdateItemsAlpha(float offset)
		{
			// Alpha 255=100%, 0=0%
			Toolbar.Background.Alpha = (int)((offset)*255);

			// Alpha 1=100%, 0=0%
			HeaderFab.Alpha = 1 - offset;
			HeaderTitle.Alpha = 1 - offset;
			HeaderSubtitle.Alpha = 1 - offset;
		}

		// Method that allows us to get the scroll Y position of the ListView
		public int getScrollY(AbsListView view)
		{
			View c = view.GetChildAt(0);
			if (c == null)
				return 0;
			int firstVisiblePosition = view.FirstVisiblePosition;
			int top = c.Top;
			int headerHeight = 0;
			if (firstVisiblePosition >= 1)
				headerHeight = this.HeaderHeight;
			return -top + firstVisiblePosition * c.Height + headerHeight;
		}

		#endregion

		/// <Docs>The options menu in which you place your items.</Docs>
		/// <returns>To be added.</returns>
		/// <summary>
		/// This is the menu for the Toolbar/Action Bar to use
		/// </summary>
		/// <param name="menu">Menu.</param>
		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.home, menu);
			return base.OnCreateOptionsMenu (menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{	
			switch (item.ItemId)
			{
			case Resource.Id.menu_share:
				Intent intent = new Intent(Intent.ActionSend);
				intent.SetType("text/plain");
				intent.PutExtra(Intent.ExtraText, "http://www.xamurais.com");
				intent.PutExtra(Android.Content.Intent.ExtraSubject, "Awesome example from Xamurais¡¡¡");
				StartActivity(Intent.CreateChooser(intent, "Share"));
				return true;
			}
			return base.OnOptionsItemSelected (item);
		}

		// The height of your fully expanded header view (same than in the xml layout)
		int HeaderHeight { get; set; }
		int MinHeaderTranslation { get; set; }

		// Header views
		View HeaderView { get; set; }
		TextView HeaderTitle { get; set; }
		TextView HeaderSubtitle { get; set; }
		FloatingActionButton HeaderFab { get; set; }
		Toolbar Toolbar { get; set; }

		//Main Listview
		ListView ListView { get; set; }

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			Toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
			//Toolbar will now take on default actionbar characteristics
			SetSupportActionBar (Toolbar);
			SupportActionBar.Title = "San Juan de los Lagos";

			HeaderHeight = Resources.GetDimensionPixelSize(Resource.Dimension.header_height);

			ListView = FindViewById<ListView> (Resource.Id.listView1);

			//Create a dummy data
			var items = new string[] { "San Juan de los Lagos","Lagos de Moreno","Zapopan", "Guadalajara",
				"Jalostotitlan", "Ajijic", "Cocula","Ciudad Guzmán","La Barca", "Ocotlán", "Puerto Vallarta",
				"Tepatitlán de Morelos"
			};

			//create a new simple adapter for our main listview
			var ListAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, items);
			ListView.Adapter = ListAdapter;
			ListView.ItemClick += ListView_ItemClick;

			// Initd the MinHeaderTranslation values

			MinHeaderTranslation = -HeaderHeight + 
				Resources.GetDimensionPixelOffset(Resource.Dimension.action_bar_height);

			// Inflate your header view
			HeaderView = LayoutInflater.Inflate(Resource.Layout.FakeHeader, ListView, false);

			// Retrieve the header views
			HeaderTitle = (TextView) HeaderView.FindViewById(Resource.Id.header_title);
			HeaderTitle.Text = "Downtown";
			HeaderSubtitle = (TextView) HeaderView.FindViewById(Resource.Id.header_subtitle);
			HeaderSubtitle.Text = "Night Life";
			HeaderFab = (FloatingActionButton) HeaderView.FindViewById(Resource.Id.header_fab);
			HeaderFab.ColorNormal = Resources.GetColor(Resource.Color.primary);
			HeaderFab.ColorPressed = Resources.GetColor(Resource.Color.primary_pressed);
			HeaderFab.Click += HeaderFab_Click;

			// Add the headerView to your listView
			ListView.AddHeaderView(HeaderView, null, false);
			ListView.SetOnScrollListener (this);
		}

		void HeaderFab_Click (object sender, EventArgs e)
		{
			Toast.MakeText (this, "Floating Action Button Clicked", ToastLength.Short).Show();
		}

		void ListView_ItemClick (object sender, AdapterView.ItemClickEventArgs e)
		{
			//Smooth scroll to the top of listview
			ListView.SmoothScrollToPosition (0);
		}
	}
}


