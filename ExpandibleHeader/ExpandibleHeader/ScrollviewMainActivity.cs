﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using com.refractored.fab;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;

namespace ExpandibleHeader
{
	[Activity (Label = "ExpandibleHeader", MainLauncher = true, Theme = "@style/MyTheme", Icon = "@drawable/icon")]
	public class ScrollviewMainActivity:ActionBarActivity, ViewTreeObserver.IOnScrollChangedListener
	{
		int MinHeaderTranslation { get; set; }
		// The height of your fully expanded header view (same than in the xml layout)
		int HeaderHeight { get; set; }

		public void OnScrollChanged ()
		{
			int scrollX = MainScrollview.ScrollX; //for horizontalScrollView
			int scrollY = MainScrollview.ScrollY; //for verticalScrollView
			Console.WriteLine(string.Format("X:{0}, Y:{1}", scrollX, scrollY));

			// Scroll ratio (0 <= ratio <= 1). 
			// The ratio value is 0 when the header is completely expanded, 
			// 1 when it is completely collapsed
			float offset = 1 - Math.Max(
				(float) (-MinHeaderTranslation - scrollY) / -MinHeaderTranslation, 0f);

			// Update items alpha from offset
			UpdateItemsAlpha(offset);
		}

		void UpdateItemsAlpha(float offset)
		{
			// Alpha 255=100%, 0=0%
			Toolbar.Background.Alpha = (int)((offset)*255);

			// Alpha 1=100%, 0=0%
			HeaderFab.Alpha = 1 - offset;
			HeaderTitle.Alpha = 1 - offset;
			HeaderSubtitle.Alpha = 1 - offset;
		}

		/// <Docs>The options menu in which you place your items.</Docs>
		/// <returns>To be added.</returns>
		/// <summary>
		/// This is the menu for the Toolbar/Action Bar to use
		/// </summary>
		/// <param name="menu">Menu.</param>
		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.home, menu);
			return base.OnCreateOptionsMenu (menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{	
			switch (item.ItemId)
			{
			case Resource.Id.menu_share:
				Intent intent = new Intent(Intent.ActionSend);
				intent.SetType("text/plain");
				intent.PutExtra(Intent.ExtraText, "http://www.xamurais.com");
				intent.PutExtra(Android.Content.Intent.ExtraSubject, "Awesome example from Xamurais¡¡¡");
				StartActivity(Intent.CreateChooser(intent, "Share"));
				return true;
			}
			return base.OnOptionsItemSelected (item);
		}

		Toolbar Toolbar { get; set; }

		ScrollView MainScrollview { get; set; }
		TextView HeaderTitle { get; set; }
		TextView HeaderSubtitle { get; set; }
		FloatingActionButton HeaderFab { get; set; }

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			SetContentView (Resource.Layout.ScrollviewMain);

			HeaderHeight = Resources.GetDimensionPixelSize(Resource.Dimension.header_height);

			MinHeaderTranslation = -HeaderHeight + 
				Resources.GetDimensionPixelOffset(Resource.Dimension.action_bar_height);

			Toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
			//Toolbar will now take on default actionbar characteristics
			SetSupportActionBar (Toolbar);
			SupportActionBar.Title = "San Juan de los Lagos";

			// Get Main Scrollview
			MainScrollview = FindViewById<ScrollView> (Resource.Id.scrollView1);

			//Trick Add OnScrollChangedListener
			MainScrollview.ViewTreeObserver.AddOnScrollChangedListener (this);

			// Retrieve the header views
			HeaderTitle = (TextView) FindViewById(Resource.Id.header_title);
			HeaderTitle.Text = "Downtown";
			HeaderSubtitle = (TextView) FindViewById(Resource.Id.header_subtitle);
			HeaderSubtitle.Text = "Night Life";
			HeaderFab = (FloatingActionButton) FindViewById(Resource.Id.header_fab);
			HeaderFab.ColorNormal = Resources.GetColor(Resource.Color.primary);
			HeaderFab.ColorPressed = Resources.GetColor(Resource.Color.primary_pressed);
			HeaderFab.Click += HeaderFab_Click;

			UpdateItemsAlpha(0);
		}

		void HeaderFab_Click (object sender, EventArgs e)
		{
			Toast.MakeText (this, "Floating Action Button Clicked", ToastLength.Short).Show();
		}
	}
}

